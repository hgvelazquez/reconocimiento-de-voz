# Entrenamiento del Modelo. 

Se incluyen tres ejemplos, uno sencillo, en formato ipynb solo
con fines de visualización. Esta basado en el archivo de 
tutorial de Nemo, pero resumiendo y modificando algunas cosas.

Los otros dos ejemplos están en `no_dropout` y `with_dropout`.
Dejé ambos para mostrar como un experimento puede cambiar sólo
modificando el YAML y para mostrar las dos formas en las 
que se pueden definir los distintos parámetros del trainer
de pytorch lightning.
