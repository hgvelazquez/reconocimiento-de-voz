"""
Script para el entrenamiento del modelo con el corpus de common-voice, 
con un conf diferente, y usando los loggers del 
Basta con correrlo como:
    $ python train-common-voice.py

POR HACER:
    Probablemente podría ser generalizado mucho más,   reciviendo
    argumentos desde la línea de comandos. 
"""
# Configuramos el uso de los GPUs asignados.
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]= "5"


# Importamos NeMo
import nemo
# Importamos el módulo de Reconocimiento de Voz
import nemo.collections.asr as nemo_asr
# Para pasar el diccionario definido por el YAML al constructor del modelo.
from omegaconf import DictConfig


# El modelo que utilizaremos es QuartzNet, que se puede instanciar con 
# EncDecCTCModel.

# Para instanciar el modelo vamos a usar un archivo YAML de configuración.


# Abrimos el archivo de configuración.
try:
    from ruamel.yaml import YAML
except ModuleNotFoundError:
    from ruamel_yaml import YAML
config_path = "/path/to/conf.yaml"

yaml = YAML(typ='safe')
with open(config_path) as f:
    params = yaml.load(f)


# Indicamos dónde se encuentran nuestros datos de train/dev/test
params['model']['train_ds']['manifest_filepath'] = "/path/to/train.json"
params['model']['validation_ds']['manifest_filepath'] = '/path/to/dev.json'
params['model']['test_ds']['manifest_filepath'] = '/path/to/test.json'

# Instanciamos el entrenador.
import pytorch_lightning as pl
# El plugin es para evitar que revise por parámetros no usados cada que
# haga el paso hacia adelante. Todos los parámetros son usados. 
from pytorch_lightning.plugins import DDPPlugin

# Instanciamos el trainer con los parámetros del yaml. 
trainer = pl.Trainer(plugins=DDPPlugin(find_unused_parameters=False), **DictConfig(params["trainer"]))
# Agregamos plugins=DDPPlugin(find_unused_parameters=False)
# para optimizar pues si no se agrega hay un paso
# adicional por todos los parámetros la red antes de cada 
# step de entrenamiento. 



# Inicializamos el Experiment Manager
# Esto es importante para el guardado de logs y 
# Tensorboard cuando se usan los parámetros del conf.yaml
from nemo.utils.exp_manager import exp_manager
exp_config = exp_manager(trainer, DictConfig(params["exp_manager"]))

# Instanciamos el modelo a partir de la configuración.
# La arquitectura está definida en el conf.yaml y se puede
# instanciar mediante EncDecCTCModel (Encoder Decoder CTC Model)
quartznet = nemo_asr.models.EncDecCTCModel(cfg=DictConfig(params['model']), trainer=trainer)

# Entrenamos el modelo.
trainer.fit(quartznet)

# Guardamos el último modelo. Si el experimento es exitoso, este es el 
# modelo final, si no, se usará para cargar al modelo alguno
# de los checkpoints guardados en el entrenamiento.
quartznet.save_to("/path/to/save/QuartznetCommonVoice.nemo")
