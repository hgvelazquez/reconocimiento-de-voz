name: &name "QuartzNet15x5_CommonVoice"

model:
  # Todos los archivos de los corpus tienen 16khz
  sample_rate: &sample_rate 16000
  # QuartzNet 15x5 repite los bloques Bi 5 veces 
  repeat: &repeat 5
  dropout: &dropout 0.0
  separable: &separable true
  labels: &labels [" ", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", 
           "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", 
           "y", "z", "á", "é", "í", "ó", "ú", "ü"]
  
  train_ds:
    manifest_filepath: ???
    sample_rate: 16000
    labels: *labels
    batch_size: 32
    trim_silence: True
    max_duration: 16.7
    shuffle: True
    is_tarred: False
    tarred_audio_filepaths: null
    tarred_shard_strategy: "scatter"
    num_workers: 16

  validation_ds:
    manifest_filepath: ???
    sample_rate: 16000
    labels: *labels
    batch_size: 32
    shuffle: False
    num_workers: 16

  preprocessor:
    _target_: nemo.collections.asr.modules.AudioToMelSpectrogramPreprocessor
    normalize: "per_feature"
    window_size: 0.02
    sample_rate: *sample_rate
    window_stride: 0.01
    window: "hann"
    features: &n_mels 64
    n_fft: 512
    frame_splicing: 1
    dither: 0.00001
    stft_conv: false

  # La parte de las capas C1, B1,...B5, C2, C3.
  encoder:
    _target_: nemo.collections.asr.modules.ConvASREncoder
    feat_in: *n_mels
    activation: relu
    conv_mask: true

    jasper:
      # Capa C1
      - filters: 256
        kernel: [33]
        stride: [2]
        dilation: [1]
        repeat: 1
        dropout: *dropout
        residual: false
        separable: *separable
      
      # Capa B1, repetida 3 veces.
      - filters: 256
        kernel: [33]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
        
      - filters: 256
        kernel: [33]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
        
      - filters: 256
        kernel: [33]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable

      # Capa B2, repetida 3 veces.
      - filters: 256
        kernel: [39]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
        
      - filters: 256
        kernel: [39]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
        
      - filters: 256
        kernel: [39]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
    
      # Capa B3, repetida 3 veces.
      - filters: 512
        kernel: [51]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable

      - filters: 512
        kernel: [51]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
        
      - filters: 512
        kernel: [51]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
        
      # Capa B4, repetida 3 veces.  
      - filters: 512
        kernel: [63]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable

      - filters: 512
        kernel: [63]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
        
      - filters: 512
        kernel: [63]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
        
      # Capa B5, repetida 3 veces.  
      - filters: 512
        kernel: [75]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable

      - filters: 512
        kernel: [75]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
        
      - filters: 512
        kernel: [75]
        stride: [1]
        dilation: [1]
        repeat: *repeat
        dropout: *dropout
        residual: true
        separable: *separable
      
      # Capa C2
      - filters: 512
        kernel: [87]
        stride: [1]
        dilation: [2]
        repeat: 1
        dropout: *dropout
        residual: false
        separable: *separable
      
      # Capa C3
      - filters: &enc_filters 1024
        kernel: [1]
        stride: [1]
        dilation: [1]
        repeat: 1
        dropout: 0.0
        residual: false
  
  # El decoder es la capa C4
  decoder:
    _target_: nemo.collections.asr.modules.ConvASRDecoder
    feat_in: *enc_filters
    num_classes: 33 # Longitud de *labels
    vocabulary: *labels

  optim:
    name: novograd
    # _target_: nemo.core.optim.optimizers.Novograd
    lr: .01
    # optimizer arguments
    betas: [0.8, 0.5]
    weight_decay: 0.001

    # scheduler setup
    sched:
      name: CosineAnnealing

      # pytorch lightning args
      monitor: val_loss
      reduce_on_plateau: false

      # Scheduler params
      warmup_steps: null
      warmup_ratio: null
      min_lr: 1e-6
      last_epoch: -1

# Estos parámetros serán pasados a un 
# Trainer de Pytorch Lightning en la Ejecución.
trainer:
  gpus: 0 # number of gpus
  max_epochs: 5
  max_steps: null # computed at runtime if not set
  num_nodes: 1
  accelerator: ddp
  accumulate_grad_batches: 1
  checkpoint_callback: False  # Provided by exp_manager
  logger: False  # Provided by exp_manager
  log_every_n_steps: 1  # Interval of logging.
  val_check_interval: 1.0 # check once per epoch .25 for 4 times per epoch
  check_val_every_n_epoch: 1

exp_manager:
  exp_dir: "/workspace/SpeechRecognition/common-voice/" 
  name: *name
  create_tensorboard_logger: True
  create_checkpoint_callback: True
  checkpoint_callback_params:
    monitor: "val_wer"
    mode: "min"
    save_top_k: 3

hydra:
  run:
    dir: .
  job_logging:
    root:
      handlers: null
