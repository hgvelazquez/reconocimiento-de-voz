"""
Script para el entrenamiento del modelo con el corpus de common-voice.
Basta con correrlo como:
    $ python train-common-voice.py

POR HACER:
    Probablemente podría ser generalizado mucho más, reciviendo
    argumentos desde la línea de comandos. 
"""

# Uso de las GPUS asignadas
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]= "5,6"

# Importamos NeMo y su módulo de reconocimiento de Voz.
import nemo
import nemo.collections.asr as nemo_asr


# El modelo que utilizaremos es QuartzNet, que se puede instanciar con 
# EncDecCTCModel.

# Para instanciar el modelo vamos a usar un archivo YAML de configuración.

try:
    from ruamel.yaml import YAML
except ModuleNotFoundError:
    from ruamel_yaml import YAML

config_path = "/path/to/conf.yaml"

yaml = YAML(typ='safe')
with open(config_path) as f:
    params = yaml.load(f)


## Definimos las rutas a nuestros archivos JSON de entrenamiento y validacion.
params['model']['train_ds']['manifest_filepath'] = "/path/to/train.json"
params['model']['validation_ds']['manifest_filepath'] = "/path/to/test.json"

# Definimos parámetros de entrenamiento
import pytorch_lightning as pl
# Para el uso en más de un GPU.
from pytorch_lightning.plugins import DDPPlugin

# En este ejemplo, definimos los parámetros de manera
# manual. En el the dropout lo hacemos con los del conf.yaml
# Es más recomendable el segundo, pues se puede usar el mismo
# script para distintos experimentos, solo cambiando el yaml 
trainer = pl.Trainer(gpus=2, accelerator="ddp", plugins=DDPPlugin(find_unused_parameters=False), max_epochs=50, log_every_n_steps=1, check_val_every_n_epoch=1)
# plugins=DDPPlugin(find_unused_parameters=false) es importante
# para el uso de más de un GPU, si no se agrega hay un paso
# adicional por todos los parámetros la red antes de cada 
# step de entrenamiento. 


# Instanciamos el modelo
from omegaconf import DictConfig

# La arquitectura está definida en el conf.yaml
quartznet = nemo_asr.models.EncDecCTCModel(cfg=DictConfig(params['model']), trainer=trainer)

# Entrenamos el modelo
trainer.fit(quartznet)

# Guardamos el modelo
quartznet.save("/path/to/save/saved.nemo")
