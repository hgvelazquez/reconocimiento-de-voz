#!/bin/bash
total=$(ls $1 | grep ".mp3" | wc -l)
current=0

for mp3file in $1/*.mp3
do
    name=$(basename $mp3file)
    filename="${name%.*}"
    newname="${2}/${filename}.wav"
    ffmpeg -i $mp3file $newname 2>/dev/null
    let "current += 1"
    echo "${current} de ${total}: ${name} convertida a wav"
done
