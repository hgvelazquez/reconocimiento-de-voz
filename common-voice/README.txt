====================================================
Archivos de Preprocesamiento para el corpus de 
Common Voice de Mozilla.
====================================================
Archivos:

----------------------------------------------------
mp3_a_wav.sh
----------------------------------------------------
El script mp3_a_wav.sh está escrito para pasar 
los archivos de audio de common-voice del formato 
mp3 al formato wav, que es  el requerido 
por NeMo.

REQUIERE DEL PROGRAMA ffmpeg

USO:
sh mp3_a_wav.sh directorio_con_mp3 directorio_wav

Donde: 

- directorio_con_mp3 es el directorio donde 
  se encuentran los archivos .mp3

- directorio_wav es el directorio donde se
  guardarán los archivos .wav (ya debe existir)   
----------------------------------------------------

----------------------------------------------------
TsvAJson.java
----------------------------------------------------
Programa de Java para procesar los archivos de texto 
en formato .tsv del corpus y convertirlos a .json 
que NeMo requiere. 

USO:
javac TsvAJson.java
java TsvAJson ruta_audios archivo_entrada nombre_salida

Donde:

- ruta_audios es la ruta absoluta donde se encuentran
  los archivos de audio.

- archivo_entrada es el archivo .tsv a procesar.

- nombre_salida es el nombre que tendrá el archivo 
  resultado
----------------------------------------------------
