/**
 * Clase de Java para procesar los archivos TSV del corpus
 * common-voice y convertirlos a archivos JSON con el formato
 * que espera NeMo. 
 */
 
/* Lectura de archivos */

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;

/* Escritura de archivos */
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;

/* Más clases de Entrada/Salida */
import java.io.IOException; 
import java.io.File;

/* Para procesar el audio (sacar longitud, en este caso)*/
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem; 
import javax.sound.sampled.AudioInputStream;

/* Para archivos de audio (necesitamos la duración) */

public class TsvAJson {
    
    private static String absolutePath;
    
    /**
     * Método para la lectura de las columnas. 
     * Nos interesan las columnas: 'path' que contiene el nombre
     * del archivo de audio (.mp3) y 'sentence' que contiene la
     * transcripción del audio.
     * @param cabecera La primer línea del archivo TSV
     * @return un arreglo de longitud 2 con los índices de las columnas 
     * buscadas; path en el índice 0, sentence en el índice 1.
     */
    private static int[] getIndicesColumnas(String cabecera) { 
        String[] columnas = cabecera.split("\t");
        int[] indices = new int[]{-1, -1};
        for (int i = 0; i<columnas.length; i++) {
            String col = columnas[i];
            if (col.trim().equalsIgnoreCase("path"))
                indices[0] = i;
            if (col.trim().equalsIgnoreCase("sentence"))
                indices[1] = i;
        }
        return indices;
    }
    
    
    /**
     * Método para obtener la duracion en segundos de un archivo de audio.
     * Utiliza la fórmula (fileLength / (frameRate*windowSize))
     * @param path el nombre del archivo de audio.
     * @return la duracion en segundos del archivo de audio.
     */
    private static double getDuracion(File audioFile) throws Exception {
        try (FileInputStream fis = new FileInputStream(audioFile);
             BufferedInputStream bis = new BufferedInputStream(fis);
             AudioInputStream aInStream = AudioSystem.getAudioInputStream(bis)) 
             
        { 
            AudioFormat formato = aInStream.getFormat();
            long longitud = audioFile.length();
            int ventanas = formato.getFrameSize();
            double frameRate = formato.getFrameRate();
            aInStream.close();
            return (longitud / (ventanas * frameRate));
        }
    }
    
    /**
     * Método para obtener la línea correspondiente del archivo JSON.
     * El formato es "{audio_filepath: "path", "duration": duracion, 
     * "text":"sentence"} "
     * @param path el nombre del archivo de audio. 
     * @oaram sentence la transcripción.
     * @return una cadena con la línea a escribir en el archivo JSON.
     */
    private static String getLinea(String path, String sentence) throws Exception{
        /* Ruta absoluta para el archivo de audio.*/
        String wavPath = path.replaceFirst("mp3", "wav");
        File audioFile = new File(new File(absolutePath), wavPath);
        /* Duración del audio */
        double duracion = 0.0;
        try {
            duracion = getDuracion(audioFile);
        } catch (IOException e) {
            throw new IOException("Problema con el archivo de audio: " + path);
        } 
        /* Construimos la línea a escribir */
        String text = sentence.replaceAll("[.,\"]", "").toLowerCase();
        String res = String.format("{\"audio_filepath\": \"%s\", ", audioFile.getPath());
        res += String.format("\"duration\": %2.7f, \"text\": \"%s\"}", duracion, text);
        return res;   
    }
    
    public static void convierte(String archivo, String salida) throws Exception{
        /* Inicializamos un BufferedReader para la lectura de archivos */
        BufferedReader in = new BufferedReader(
            new InputStreamReader(
                new FileInputStream(archivo)));
        /* Inicializamos un BufferedWriter para la escritura de archivos */
        BufferedWriter out = new BufferedWriter(
            new OutputStreamWriter(
                new FileOutputStream(salida)));
        
        int[] indices = getIndicesColumnas(in.readLine());
        
        /* Procesamos cada una de las líneas */
        String linea, lineaJson, path, sentence;
        String[] entradas;
        
        while ((linea = in.readLine()) != null){
            entradas = linea.split("\t");
            path = entradas[indices[0]];
            sentence = entradas[indices[1]];
            lineaJson = getLinea(path, sentence);
            out.write(lineaJson);
            out.newLine();
        }
        
        /* Cerramos los streams de Entrada/Salida. */
        in.close();
        out.close();
    }
    
    /* Método main */
    public static void main(String[] args){
        
        if (args.length < 3){
            System.err.printf("\nUSO: java TsvAJson ruta_audios archivo_entrada nombre_archivo_salida\n");
            System.exit(1);
        }
        
        absolutePath = args[0];
        
        try{
            convierte(args[1], args[2]);
        } catch (Exception ioe) {
            System.err.printf("Error al procesar el archivo: %s\n", args[1]);
            ioe.printStackTrace();
        }    
    }
}
