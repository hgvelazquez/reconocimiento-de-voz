# Evaluación del Modelo. 

**Este directorio no contiene archivos de mi autoría salvo
el instructivo y este README.md**

Este directorio contiene todos los scrpits necesarios para la 
evaluación del modelo. Fueron extraídos del [repositorio
oficial de Nemo](https://github.com/NVIDIA/NeMo), de la rama correspondiente instalada en la 
versión de la imagen construida del contenedor. 

En el servidor del INAOE se puede utilizar esta imagen
ya construida, está bajo el nombre nemo. Sin embargo, 
si se quiere volver a crear la imagen, será necesario
revisar la versión instalada de Nemo y descargar los
scripts correspondientes a esta versión.

Para un breve instructivo de cómo ejecutar el script
de evaluación con el modelo de lenguaje, checar el 
archivo correspondiente.
