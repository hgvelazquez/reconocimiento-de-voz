"""
Este script es el usado para hacer la separación de los corpus
de DIMEx100_LIGHT y DIMEx100_NINOS en entrenamiento y evaluación,
de manera que el de evaluación contenga las mismas frases dichas
por los niños y por los adultos.
"""


# Se leen las líneas del archivo de DIMEx100 Light
with open("/001/usuarios/carlos.hernandez/SPEECH_DB/SPANISH/Train/SHAREABLE/DIMEx100_LIGHT/DIMEx100_LIGHT.json", "r") as f:
    lineslight = f.readlines()


# Creamos una lista de formato (indice, texto)
texts = []
for (i,l) in zip(range(len(lineslight)), lineslight):
    text = l.split(":")[-1][2:-3]
    texts.append((i, text))

# Creamos un diccionario con formato frase: número de hablantes
speakers = {}
for (i,t) in texts:
    if speakers.get(t):
        speakers[t] = speakers[t]+1
    else:
        speakers[t] = 1

# Obtenemos la lista de líneas cuyo texto no se repite y otra con las que sí se repiten
non_repeated = []
check = []
repeated = []
        
for (i,t) in texts:
    if speakers[t] == 1:
        non_repeated.append(lineslight[i])
    else:
        if not (t in repeated):
            repeated.append(t)
    check.append(t)


# Escribimos los no repetidos a un archivo
with open("/001/usuarios/hgv/SpeechRecognition/DIMEx100/DIMEx100_LIGHT.json", "w") as f2:
    for t in non_repeated:
        f2.write(t)   
        
# Repetimos el proceso con el DIMEx100 Niños
with open("/001/usuarios/carlos.hernandez/SPEECH_DB/SPANISH/Train/SHAREABLE/DIMEx100_NINOS/DIMEx100_NINOS.json", "r") as f:
    linesninos = f.readlines()
        
for (i,l) in zip(range(len(linesninos)), linesninos):
    text = l.split(":")[-1][2:-3]
    if text == "la construcción de la alteridad en comunidades indígenas en américa latina":
        print(l) 
        
rep = ["todos los campos son opcionales puedes contestar lo que desees",
"la construcción de la alteridad en comunidades indígenas en américa latina",
"el diario el mundo se edita en madrid españa",
"destino definitivo en el centro",
"peligros de la sociedad de consumo",
"ayuda para la presentación telemática de declaraciones",
"lituania y don josé ballesta rector de la universidad de murcia",
"salen los universitarios preparados para el mercado laboral",
"garantice la seguridad de su red",
"cuál es la diferencia de este gobierno",
"avancemos con el resto de las opciones",
"todo sobre la guerra contra el terrorismo",
"realización de programas de radio y televisión",
"los principales vestigios arqueológicos que se localizan en el estado de campeche son",
"gracias a todos los que nos apoyan asistiendo a los conciertos",
"los aumentos en el costo de la energía afectarán a tu bolsillo",
"por qué no se puede realizar el aborto",
"introduzca su nombre de usuario y contraseña y pulse el botón",
"recopilación de firmas en contra de la extrema derecha de austria",
"todos los campos son opcionales puedes contestar lo que desees",
"la construcción de la alteridad en comunidades indígenas en américa latina",
"el diario el mundo se edita en madrid españa",
"destino definitivo en el centro",
"peligros de la sociedad de consumo",
"ayuda para la presentación telemática de declaraciones",
"lituania y don josé ballesta rector de la universidad de murcia",
"salen los universitarios preparados para el mercado laboral",
"garantice la seguridad de su red"]

#

for r in rep:
    for i,t in texts:
        if t == r:
            print(lineslight[i])

            
textninos = []
for (i,l) in zip(range(len(linesninos)), linesninos):
    text = l.split(":")[-1][2:-3]
    textninos.append((i, text))

speakersn = {}
for (i,t) in textninos:
    if speakersn.get(t):
        speakersn[t] = speakersn[t]+1
    else:
        speakersn[t] = 1

nrninos = []
repninos = []   
for (i,t) in textninos:
    if speakersn[t] == 1:
        nrninos.append(linesninos[i])
    else:
        if not (t in repninos):
            repninos.append(t)
        

# Escribimos los no repetidos a un archivo
with open("/001/usuarios/hgv/SpeechRecognition/DIMEx100/DIMEx100_NINOS.json", "w") as f2:
    for t in nrninos:
        f2.write(t)   

# El primero de DIME100, el resto de DIME100NINOS, son el 100+id
# Como comparten frases no queremos que queden en la prueba, pues
# esos conjuntos queremosq que sean idénticos
missing_audio_subjects = [35, 6, 8, 4, 40, 48, 50, 57, 88]

indexes = list(range(1, 101))
for m in missing_audio_subjects:
    indexes.remove(m)

np.random.seed(42)
mixed = np.random.permutation(indexes)

train_subjects = 80-len(missing_audio_subjects)
train = list(mixed[:train_subjects]) + missing_audio_subjects

test = list(mixed[train_subjects:])

def get_index(line, ninos=False):
    idn = int(line.split(":")[1].split("/")[-1].split("_")[2])
    if ninos:
        return idn-100
    else:
        return idn

false_reps = {}
for (i, t) in textninos:
    if (t in repeated) and (t not in repninos):
        index = get_index(linesninos[i], ninos=True)
        false_reps[t] = index
        print(f"{index}, {t}")

non_repeatedw = []
for (i, t) in texts:
    index = get_index(lineslight[i])
    if false_reps.get(t) == index:
        non_repeatedw.append(lineslight[i])
        false_reps[t] = 0

trainlines = []
testlines = []
for l in non_repeated:
    idx = get_index(l)
    if idx in test:
        testlines.append(l)
    else:
        trainlines.append(l)

        
with open("/001/usuarios/hgv/SpeechRecognition/DIMEx100/DIMEx100_LIGHT_Train1.json", "w") as f:
    for t in trainlines: 
        f.write(t)

with open("/001/usuarios/hgv/SpeechRecognition/DIMEx100/DIMEx100_LIGHT_Test.json", "w") as f:
    for t in testlines: 
        f.write(t)
       
trainlinesn = []
testlinesn = []
for l in nrninos:
    idx = get_index(l, ninos=True)
    if idx in test:
        testlinesn.append(l)
    else:
        trainlinesn.append(l)

with open("/001/usuarios/hgv/SpeechRecognition/DIMEx100/DIMEx100_NINOS_Train.json", "w") as f:
    for t in trainlinesn: 
        f.write(t)

with open("/001/usuarios/hgv/SpeechRecognition/DIMEx100/DIMEx100_NINOS_Test.json", "w") as f:
    for t in testlinesn: 
        f.write(t)

