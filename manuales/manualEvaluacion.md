# Manual para la evaluación de modelos
Primero se detallara el procedimiento para evaluar el modelo usando un 
_rescoring_ con un modelo de lenguaje de n-gramas. 

### Prerrequisitos

##### Swig
Lo primero que hay que realizar es instalar [Swig](http://www.swig.org/). Si 
está usando el contenedor de Docker oficial o el construido con el Dockerfile
de este repositorio, se puede instalar con los siguientes comandos dentro del 
contenedor:
```
apt update
apt install swig
```
Si no está usando un contenedor necesitará de permisos de _root_ e instalarlo
con el manejador de paquetes adecuado a la distribución que esté utilizando. 

##### Script de Nemo
Una vez que se ha instalado swig, lo que sigue es ejecutar el _script_ de bash ```install.sh```. En este repositorio se incluye el que corresponde a la 
versión de Nemo usada para el contenedor, pero puede obtener el script desde
el [repositorio oficial de Nemo](https://github.com/NVIDIA/NeMo/tree/main/scripts/asr_language_modeling/ngram_lm). 
Esto se realiza con los comandos:

```
chmod +x install.sh
./install.sh
```

El primero de los comandos es para dar al archivo permisos de ejecución, y 
el segundo es para ejecutarlo. 

Una vez que se haya ejecutado este script, basta con descargar los archivos 
`kenlm_utils.py` y `eval_beamsearch_ngram.py`, que de igual forma han sido
incluidos en este repositorio, pero se pueden obtener del repositorio de NeMo.

##### Otros prerrequisitos
Además del un archivo en formato `.nemo` con el modelo a evaluar, son necesarios:
- Un archivo binario con el modelo de n-gramas previamente entrenado. 
- Un archivo en formato `.json` con el formato `{audio_filepath:'/path/file.wav', duration:xx, transcription:'lorem ipsum'}` con los archivos de audio y las transcripciones que se usarán para la evaluación.


### Evaluación

El comando siguiente imprime el WER resultante con `greedy` decoding (sin hacer
el _rescoring_, solo la red neuronal) y el WER resultante de hacer el
_rescoring_ usando el modelo de lenguaje con el algoritmo `beam search`:

```
python eval_beamsearch_ngram.py \
        --nemo_model_file /path/to/model.nemo \
        --input_manifest /path/to/test.json \
        --kenlm_model_file /path/to/language-model.binary \
        --acoustic_batch_size 32 \
        --beam_width 128 \
        --beam_alpha 1.0 \
        --beam_beta 0.5 \
        --preds_output_folder /path/to/save/transcripts \
        --decoding_mode beamsearch_ngram
```

A continuación se desglosan los parámetros usados:
- `--nemo_model_file` para especificar la ruta donde se encuentra el archivo nemo.
- `--input_manifest` para la ruta del json con los datos a usar para la evaluación.
- `--kenlm_model_file` para la ruta del archivo binario con el modelo del lenguaje.
- `--acoustic_batch_size` Es el tamaño del batch que se usará para cargar los datos.
- `--decoding_mode beamsearch_ngram` es para indicar que se va a utilizar un modelo de lenguaje para el decoding. No lo cambie si quiere que en efecto sea usado. 
- `--preds_out_folder` para la dirección donde guardar las transcipciones resultantes. Se puede omitir si no se desean guardar.
- `--beam_width`, `--beam_alpha` y `--beam_beta` son los hiperparámetros del algoritmo **beam search**

Dependiendo de su ambiente de desarrollo, puede que deba usar `python3` en lugar de `python` como comando para ejecutar scripts de Python 3. 

Como nota importante. los parámetros `beam_width`, `beam_alpha` y `beam_beta`
admiten de más de un valor separados por un espacio (por ejemplo, `--beam_alpha 2.0 1.0`). Si se pasan más de un valor se hará un _grid search_ con todas 
las combinaciones de estos tres hiperparámetros. 
