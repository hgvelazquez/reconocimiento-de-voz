{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Manual de Entrenamiento de un Modelo con Nemo\n",
    "\n",
    "Este cuaderno contiene el código necesario para realizar un entrenamiento básico, reemplazando las rutas adecuadas para los archivos de entrenamiento. \n",
    "\n",
    "Lo ideal es correr el entrenamiento directamente desde la terminal usando docker, para no depender de que el navegador permanezca abierto o que la sesión de ssh al servidor se mantenga. \n",
    "\n",
    "Se presentan todo lo necesario para echar a correr tu primer entrenamiento con Nemo, resaltando partes importantes en la configuración y poniendo ejemplos. \n",
    "\n",
    "Además se presentan fragmentos de código ilustrativos.  \n",
    "Esto con la intención de que pueda servir a quienes se lleguen a integrar al proyecto posteriormente. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preparaciones previas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Para esta tarea utilizaremos [Nvidia NeMo](https://developer.nvidia.com/nvidia-nemo) para entrenar una red neuronal con la arquitectura de [Quartznet](https://arxiv.org/pdf/1910.10261.pdf). \n",
    "\n",
    "Los datos de entrenamiento y validación no se incluyen, pero se recomienda usar un corpus pequeño de ejemplo.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Configuración de los archivos del Corpus\n",
    "\n",
    "Previamente, para poder entrenar un modelo con NeMo se necesitan procesar los archivos de la siguiente forma:\n",
    "- Los archivos de audio deben ser convertidos a formato wav, este es el único formato posible hasta la versión 1.0.3 de Nemo.\n",
    "- Se deben poner las transcripciones en archivos de formato .json, donde cada entrada tiene la forma:\n",
    "   ```\n",
    "   {audio_filepath:\"/ruta/a/audio.wav, duration:in_seconds, text:\"transcription of audio file\"}\n",
    "   ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Configuración de recursos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En la plataforma de INAOE tenemos GPU's asignados, debemos indicar cuáles son para que nuestro código ocupe únicamente los recursos asignados. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "os.environ[\"CUDA_DEVICE_ORDER\"]=\"PCI_BUS_ID\"\n",
    "os.environ[\"CUDA_VISIBLE_DEVICES\"]= \"5,6\" # Separar cada GPU con coma"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Podemos verificar que en efecto ha funcionado:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "torch.cuda.device_count() # La cuenta debe ser igual al número de GPUs asignados "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Datos de entrenamiento"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Los datos para un modelo de reconocimiento de voz consisten de un archivo de audio de corta duración (en el caso de Nemo, específicamente en formato wav), en donde se pronuncia una frase de la cual tenemos una transcripción.\n",
    "Esta transcripción puede ser obtenida de dos formas:\n",
    "\n",
    "- Usando frases previamente definidas que son leídas para obtener los archivos de audio. Ejemplos: Mozilla Common Voice, Dime100.\n",
    "- Haciendo una transcripción manual. Ejemplo: CIEMPIESS \n",
    " \n",
    "\n",
    "En general el primer tipo es una tarea más sencilla ya que en no hay espontaneidad, pausas largas o muletillas en el audio.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Analizando un ejemplo en particular"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import librosa\n",
    "import IPython.display as ipd\n",
    "\n",
    "# Reemplaza esta ruta por cualquier ejemplar del corpus que hayas elegido usar como ejemplo.\n",
    "ejemplo = '/ruta/a/common-voce/files/example_file.wav'\n",
    "audio, sample_rate = librosa.load(ejemplo)\n",
    "\n",
    "ipd.Audio(ejemplo, rate=sample_rate)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Una forma sencilla de visualizar esta onda de audio es mediante graficando la onda de sonido a lo largo del tiempo:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import librosa.display\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Gráfica de la onda\n",
    "plt.rcParams['figure.figsize'] = (15,7)\n",
    "plt.title('Onda de Sonido del Ejemplo')\n",
    "plt.ylabel('Amplitud')\n",
    "\n",
    "_ = librosa.display.waveplot(audio)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora bien, la manera en la que nuestro modelo podrá procesar el audio es usando la información de las frecuencias de la onda sonora a lo largo del tiempo. \n",
    "\n",
    "Esta información la extraemos en un **espectograma**.\n",
    "Un espectograma nos da una representación de el nivel de energía (la amplitud visualizada en la gráfica anterior) desglosada por cada frecuencia distinta a lo largo del tiempo. \n",
    "\n",
    "Nuestro modelo va a recibir un **espectograma de Mel** cuya única diferencia con un espectograma regular es que hace un reescalamiento de las frecuencias para que estén a una distancia equivalente a como las distingue el oído humano. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "# Obtenemos el Espectograma de Mel con Librosa\n",
    "mel_spec = librosa.feature.melspectrogram(audio, sr=sample_rate)\n",
    "mel_spec_db = librosa.power_to_db(mel_spec, ref=np.max) # Escalamiento logarítmico de las frecuencias\n",
    "\n",
    "librosa.display.specshow(\n",
    "    mel_spec_db, x_axis='time', y_axis='mel')\n",
    "plt.colorbar()\n",
    "plt.title('Mel Spectrogram');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preparando el modelo"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Importamos NeMo\n",
    "import nemo\n",
    "# Importamos el módulo de Reconocimiento de Voz\n",
    "import nemo.collections.asr as nemo_asr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "El modelo que utilizaremos en este ejemplo es QuartzNet, en su modalidad 5x5. \n",
    "En NeMo, la clase que necesitamos es EncDecCTCModel, que quiere decir *Encoder-Decoder Connectionist Time Classificator* \n",
    "\n",
    "Para instanciar el modelo vamos a usar un archivo YAML de configuración, donde describimos cada una de las capas que tendrá el modelo. Esto nos permite describir el modelo a muy alto nivel y hará que instanciarlo sea muy sencillo. \n",
    "\n",
    "Este archivo lo he configurado previamente y la configuración se ha hecho con la descripción del [artículo de QuartzNet](https://arxiv.org/pdf/1910.10261.pdf), con el tamaño de 5x5. Dentro del Yaml cada capa tiene un comentario con su nombre dentro de la tabla del artículo. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Iniciamos cargando la configuración** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    from ruamel.yaml import YAML\n",
    "except ModuleNotFoundError:\n",
    "    from ruamel_yaml import YAML\n",
    "config_path = '/ruta/a/archivo/conf.yaml'\n",
    "\n",
    "yaml = YAML(typ='safe')\n",
    "with open(config_path) as f:\n",
    "    params = yaml.load(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Podemos visualizar el YAML con más detenimiento si lo deseamos"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat 'examples/config.yaml'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A este YAML le faltan las configuraciones necesarias para el entrenamiento y la validación. Tenemos que definir los archivos JSON de donde obtendrá estos conjuntos de datos. \n",
    "\n",
    "Las siguientes celdas de código pueden ser omitidas si agregas las rutas directamente en el YAML. \n",
    "**Importante:** Si no están configuradas estas dos rutas, el modelo no puede instanciarse correctamente"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Reemplaza con las rutas a los archivos JSON de entrenamiento y validacion del corpus de ejemplo.\n",
    "train_manifest = \"/ruta/datos/train.json\"\n",
    "test_manifest = \"/ruta/datos/test.json\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Agregamos estas rutas a la configuración\n",
    "params['model']['train_ds']['manifest_filepath'] = train_manifest\n",
    "params['model']['validation_ds']['manifest_filepath'] = test_manifest"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Podemos modificar cada uno de los parámetros ya cargados como con cualquier diccionario de Python, por ejemplo:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params['model']['train_ds']['num_workers']=8\n",
    "params['model']['validation_ds']['num_workers']=8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p style=\"font-size: 1.5em\"> <strong> ¡IMPORTANTE! </strong></p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "El parámentro `num_workers` de `train_ds` y `validation_ds` debe ser configurado, de preferencia con un número alto, dependiendo de los recursos del servidor. Un buen número es 16. \n",
    "\n",
    "Si no se configura, por defecto será 1, y esto puede ser un extremo cuello de botella en el entrenamiento del modelo, ya que es tarea del CPU cargar los datos de la memoria. Si tenemos pocos workers, puede tomar más tiempo cargar los datos que hacer el forward y backward del modelo. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Parámetros para el entrenamiento \n",
    "\n",
    "Para realizar el entrenamiento se recomienda usar un *Trainer* de *Pytorch Lightning*. Esto te permite ahorrar escribir el loop de entrenamiento y cambiarlo por una sola línea. Además de eso, tiene una integración muy sencilla con callbacks y loggers, en particular permite una configuración de **Tensorboard** muy sencilla. Si has usado Keras le podrás encontrar la similitud"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Biblioteca importante para pasar el dict como keyword arguments\n",
    "from omegaconf import DictConfig"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pytorch_lightning as pl\n",
    "trainer = pl.Trainer(**DictConfig(params[\"trainer\"]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En este caso hemos definido la mayoría de los *kwargs* del constructor de Trainer en el conf.yaml\n",
    "Sin embargo, hay que notar que se pueden definir desde el lado de código también."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p style=\"font-size: 1.5em\"> <strong> ¡IMPORTANTE! </strong></p> "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Si vas a usar más de un GPU, es importante agregar:\n",
    "- El **accelerator** que da el algoritmo a utilizar para hacer uso de varios GPUs. Se debe usar ddp. Este se puede agregar desde el yaml, agregando al *trainer* la etiqueta: `accelerator: ddp`\n",
    "- Un DDPPlugin para evitar que en cada step de entrenamiento se busquen los parámetros no usados. Esto se hace desde código, como en el siguiente ejemplo:\n",
    "\n",
    "Ojo, ese código no corre dentro de un notebook, pero si lo ejecutas desde terminal no habrá problema."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer = pl.Trainer(gpus=2, plugins=DDPPlugin(find_unused_parameters=False), **DictConfig(params[\"trainer\"]))\n",
    "\"\"\"\n",
    "Recuerda que no puedes definir parámetros por dos medios diferentes. \n",
    "Para que el código funcione, O modificas el yaml quitando la etiqueta gpu\n",
    "O quitas el kwarg gpu del código. \n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Configuración de Tensorboard"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pytorch lightning por defecto configura un logger a tensorboard. Este logger guarda los datos en el directorio `./lightning_logs/version_x`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Si se quiere usar esto, es importante que no se pase ninguna etiqueta del yaml que tenga que ver con tensorboard. Si se pasa alguna, esta configuración por defecto deja de funcionar. \n",
    "\n",
    "Si dejas todo en código por ejemplo, el siguiente trainer hará los logs por defecto:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer = pl.Trainer(gpus=1, max_epochs=50, log_every_n_steps=1, check_val_every_n_epoch=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora, lo ideal es manejar todo desde el YAML, y este nos brinda una sección para el *experiment manager*, bajo la etiqueta **exp_manager**. Para esto es importante poner las siguientes etiquetas:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "trainer:\n",
    "  # Etiquetas antes ...\n",
    "  checkpoint_callback: False  # Reemplazado por exp_manager\n",
    "  logger: False  # Reemplazado por exp_manager\n",
    "  log_every_n_steps: 1  # Intervalo de logging. \n",
    "  val_check_interval: 1.0 # Cada cuántas épocas se hace la validación ejemplo: epoch .25 son 4 veces por epoch\n",
    "  check_val_every_n_epoch: 1 # Misma sintáxis que etiqueta anterior\n",
    "  # Etiquetas después ...\n",
    "\n",
    "exp_manager:\n",
    "  exp_dir: \"/path/to/where/you/want/your/logs\" \n",
    "  name: *name\n",
    "  create_tensorboard_logger: True\n",
    "  create_checkpoint_callback: True\n",
    "  checkpoint_callback_params: \n",
    "  # Aquí defines qué métrica es la que importa para early stopping, mejores modelos, y otros callbacks. \n",
    "    monitor: \"val_wer\" \n",
    "    mode: \"min\"\n",
    "    save_top_k: 3\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Y con esos parámetros, se agrega el siguiente código:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Lo que ya teníamos\n",
    "trainer = pl.Trainer(**DictConfig(params[\"trainer\"]))\n",
    "\n",
    "# El experiment manager\n",
    "from nemo.utils.exp_manager import exp_manager\n",
    "exp_config = exp_manager(trainer, DictConfig(params[\"exp_manager\"]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Eso es todo lo que hay que agregar para que funcione"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Entrenando al modelo"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En este caso, quartznet es un EncDecCTCModel, pues consta de un **Enc**oder, un **Dec**oder y ocupa **CTC** loss\n",
    "\n",
    "Toda la información del modelo se definió en el conf.yaml (capa por capa).\n",
    "\n",
    "Y además de eso pasamos el trainer que vayamos a usar como argumento."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "quartznet = nemo_asr.models.EncDecCTCModel(cfg=DictConfig(params['model']), trainer=trainer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Así como Tensorflow tiene Keras, y se puede hacer model.fit(), pytorch lightning nos permite entrenar con una única línea de código:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer.fit(quartznet)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "No olvides guardar tu modelo al terminar"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "quartznet.save_to(\"/path/to/save/QuartznetCommonVoice.nemo\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Como recomendación, para sesiones largas de entrenamiento es mejor utilizar un script de Python y no un cuaderno de Jupyter, pues el primero se puede dejar corriendo en un contenedor de Docker y puede ser monitoreado en cualquier momento, mientras que el segundo requiere de tener un kernel corriendo, lo cual implica tener una pestaña de navegador y una conexión SSH abiertas --lo cual no es muy práctico si se espera dejar corriendo el entrenamiento por días o semanas--."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
