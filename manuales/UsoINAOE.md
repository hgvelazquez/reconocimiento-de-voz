# Uso de la plataforma de INAOE para el entrenamiento de modelos

## Prerrequisitos
Para hacer uso del servidor remoto del INAOE es necesario tener en tu computadora un **cliente ssh**. En las mayoría de las distribuciones de Linux y en MacOS el cliente viene ya instalado; en el caso de Windows es necesario instalar uno. El más utilizado para este sistema operativo es [Putty](https://www.putty.org/).

El cliente ssh te permitirá conectarte al servidor e interactuar con él a través de la línea de comandos.

## Conexión al Servidor
Si tienes acceso a la plataforma, te harán llegar tu usuario y contraseña, así como la dirección IP del servidor y el puerto al cual te podrás conectar por ssh.

Con estos datos puedes conectarte al servidor con el siguiente comando:
```
ssh user@ip.address -p port
```
Sustituyendo:

- user por tu usuario.
- ip.address por la dirección ip del servidor.
- port por el puerto del servidor.

Al correr este comando se te pedirá tu contraseña y se iniciará una conexión con el servidor.

A partir de este punto, la terminal donde corriste el comando está en una sesión dentro del servidor remoto. Esto significa que los comandos que ejecutes en esta terminal son ejecutados en el servidor, no en tu máquina local.

Puedes cerrar la conexión usando el comando `exit` o simplemente presionando `Ctrl+d`

Es recomendable que la primera vez que entres cambies tu contraseña por una más segura, utilizando el comando `passwd`

### Cómo utilizar Jupyter y otras aplicaciones de interfaz web

Muchas de las aplicaciones que solemos utilizar en deep learning cuentan con una interfaz web que corre sobre el navegador. Tal es el caso de **Jupyter notebooks** y **Tensorboard**.

Para que estas aplicaciones se puedan ejecutar, a través de la conexión `ssh` es necesario establecer un **Tunnel**, o como también se le llama, hacer **port forwarding**.

La manera más sencilla de lograr esto es mediante la bandera `-L` dentro del comando `ssh` al momento de establecer la conexión

La sintaxis es la siguiente:
```
ssh -L puerto_local:0.0.0.0:puerto_destino user@ip.address -P puerto_ssh
```
Donde:

- `puerto_ssh` es el mismo puerto que teníamos en la sección anterior. Es el puerto donde `ssh` está configurado en el servidor.
- `puerto_local` es el puerto desde el cual puedes acceder a la aplicación en tu máquina. Es decir, aquel que pones en la url del navegador: *http://localhost:puerto_local*
- `puerto_destino` es el puerto donde está corriendo la aplicación en el servidor. Es el puerto que ingresas al ejecutar el comando, por ejemplo `jupyter-notebook --port=puerto_destino`
- Se ocupa `0.0.0.0` pues todas las aplicaciones se ejecutan directamente en el servidor.

Es importante notar que puedes repetir la bandera -L cuantas veces quieras, por si requieres conectarte a más de un puerto.

#### Ejemplo práctico
Supongamos que queremos abrir un *notebook* para ejecutar nuestro código de python.

Nos conectamos al servidor con el comando:
```
ssh -L 9000:0.0.0.0:9001 user@ip.address -p 22001
```
El puerto 22001 es el puerto configurado para la conexión ssh en el servidor remoto.

Una vez conectado, debes ejecutar jupyter usando el puerto 9001:

```
jupyter-notebook --ip=0.0.0.0 --port=9001 --no-browser
```

Finalmente, abres tu navegador y pones la dirección
```
localhost:9000/?token=123456789abcdef
```
Por supuesto, reemplazando **123456789abcdef** por el token que el comando de jupyter imprime en la terminal.



Con esto es más que suficiente para poder trabajar con estas aplicaciones en el servidor remoto, pero si te interesa aprender más sobre **port forwarding** [este es un excelente recurso.](https://linuxize.com/post/how-to-setup-ssh-tunneling/)

## Usando Docker

Ya con lo de la sección anterior podrías ejecutar tu código, sin embargo hay un pequeño problema. La sesión de ssh debe permanecer abierta durante toda la ejecución de tu código. Si tu programa no tarda mucho en ejecutar esto no será un problema, pero como seguro sabes, entrenar una red neuronal muy grande con muchos datos puede llevar días.

Hay muchas soluciones a este problema. La que yo usé durante mi proyecto, y que no requiere que instales nada adicional a lo que hay en el servidor es [Docker](https://www.docker.com/). Esto funciona gracias a que los comandos ejecutados a través de Docker son asociados al *Docker daemon* y no a tu usuario o sesión de ssh.

**El resto de este manual te será útil hasta el momento en que ya tengas listo tu script de entrenamiento**

### ¿Qué imagen de Docker usar?

El servidor de INAOE (a inicios de 2022) cuenta con un problema en su instalación que desafortunadamente no permite el uso de la imagen oficial de Nemo.

En su lugar, yo creé una imagen que puedes encontrar en el servidor bajo el nombre **nemo**.

Esta imagen fue creada a través el `Dockerfile` que viene en este repositorio. Si quisieras modificarlo y crear tu propia imagen, basta con copiar tu archivo modificado al servidor, y desde el directorio donde esté localizado dicho archivo, ejecutar el comando `docker build -t image-name .`

El punto al final es parte del comando.
Por supuesto, debes cambiar `image-name` por el nombre que quieras ponerle a tu imagen.

### ¿Cómo ejecutar mi script de entrenamiento?
Para ejecutar tu script de entrenamiento deberás hacerlo dentro del comando `docker run`. Este comando tiene muchas opciones y banderas que puedes agregarle. A continuación te presento un *template* del comando que creo que puede resultarte útil:

```
docker run -it -d --rm \
           -v /path/to/training/data:/path/to/data/in/container \
           -p puerto_fuera:puerto_contenedor \
           --shm-size=2g \
           --ulimit memlock=-1 \
           --ulimit stack=67108864 \
           --name containerName \
           imageName \
           python3 /path/to/scipt/train.py
```
Te explico cada una de las banderas y opciones del comando a continuación.

#### -it
Es para inicializar una entrada y salida estándar en el contenedor.

#### -d
Significa *detached* Es para ejecutar el comando en el fondo.

Nota: si requieres entrar al contenedor a ejecutar varios comandos, deberás quitar esta bandera.

#### -rm
Esta bandera s para que el contenedor sea eliminado al terminar la ejecución del comando.
#### -v
Esta bandera s para que el contenedor tenga acceso a un directorio.

La sintaxis para esta bandera es `-v /path/in/computer:/path/to/data/in/container`. Esto quiere decir que el directorio en tu sistema de archivos `/path/in/computer` estará disponible en el contenedor. Dentro del contenedor, ese directorio queda en `/path/to/data/in/container`.

Así, por ejemplo, el archivo `/path/in/computer/readme.txt` dentro del contenedor se vuelve `/path/to/data/in/container/readme.txt`. Esto aplica tanto para lecturas como para escrituras.

Esta bandera la puedes repetir tantas veces quieras para dar acceso a cuantos directorios requieras. Yo recomiendo usarlo para dos casos:

- El directorio donde están los datos de entrenamiento, para que se puedan leer dentro del contenedor.
- El directorio donde quieras que se realicen las escrituras a archivos realizadas por tu programa.

#### -p
Permite mapear puertos dentro del contenedor. La sintaxis es `-p puerto_fuera:puerto_contenedor`. Por ejemplo, si usas `-p 9998:9999`, entonces, si dentro del contenedor ejecutas `jupyter-notebook --port=9999`, esto quiere decir que jupyter se está ejecutando dentro del contenedor en el puerto 9999, pero en el **servidor**, que es con quien tienes comunicación por ssh, está ejecutándose en el puerto 9998.

#### --shm-size
Para dar un tamaño específico al `shared memory` que utilizará el contenedor. Por defecto, docker usa 64 MB. Por razones de rendimiento, yo usé 2 gigabytes, pero se puede modificar de acuerdo a tus necesidades y recursos (`shared_memory` vive en la RAM).

#### --ulimit
Esta bandera la usamos para la asignación de recursos al contenedor. `memlock=-1` quiere decir que dejamos que cada proceso dentro del contenedor no tenga límite de memoria local y `stack=67108864` significa que estamos dando 64 GB como limite a la memoria del stack de cada proceso.

#### --name
Es para darle un nombre específico al contenedor.

#### imagen y comando

Las últimas dos opciones del comando son `imageName` para pasar el nombre de la imagen a ser utilizada y el comando que quieres ejecutar. En el ejemplo, `python3 /path/to/sctipt/train.py`. Este comando puede ser reemplazado por aquel que necesites.

Si quieres entrar al contenedor para ejecutar varios comandos, puedes omitir la última línea del comando de ejemplo.

Este comando debe ser suficiente para lo que necesitas. Recuerda que puedes agregar las banderas -v y -p cuantas veces lo requieras. 
Si necesitas modificar el comando de ejemplo para algo más específico, puedes revisar [la documentación](https://docs.docker.com/engine/reference/run/).


### ¿Cómo puedo monitorear el entrenamiento?
La recomendación es que configures tu script de entrenamiento para poder usar **tensorboard** que te permitirá ver en tiempo real y de forma gráfica el proceso de entrenamiento.

Para realizar esto con Nemo, puedes checar el manual que incluyo acerca de los básicos en Nemo.

Si hiciste bien la configuración, entonces pueder ejecutar el entrenamiento con Docker y dejar el proceso corriendo. Cuando desees revisar cómo va el entrenamiento:

- Te conectas al servidor por medio de ssh. Recuerda usar la opción `-L`, por ejemplo con `-L 7000:0.0.0.0:7000`.
- Entra a un contenedor donde hayas instalado *tensorboard* (por ejemplo, con el comando ` conda install -c conda-forge tensorboard `). Esto lo haces con `conda activate envName`, donde envName es el nombre del contenedor.
- Ejecutas tensorboard, con el comando (recuerda usar el puerto que definiste en el comando ssh, yo seguiré con el ejemplo):
```
tensorboard --logdir /path/configured/in/script/ --host 0.0.0.0 --port 7000
```
- Finalmente, abre en tu navegador la dirección `localhost:7000` (recuerda que 7000 es ejemplo, usa el puerto que definiste en -L).


Sin embargo, si prefieres monitorear la salida producida en línea de comandos, puedes revisar los **logs** de Docker, con el comando:
```
docker logs -f containerName
```

donde `containerName` es el nombre que pusiste al contenedor cuando ejecutaste el comando `docker run`
