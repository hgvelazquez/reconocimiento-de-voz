# syntax=docker/dockerfile:experimental

# Dockerfile para construir un contenedor para NVIDIA NEMO tomando como
# base el contenedor de TensorRT. 

# No podemos usar el contenedor de Nemo directamente debido al bug con el 
# sistema de overlay2 con RedHat y sistemas de almacenamiento xfs con 
# d_type=false. 
# Ver por ejemplo: https://github.com/docker/docker.github.io/issues/5253 

# Como este es el caso con el servidor que utilizamos, la instalación en este
# Dockerfile se hace con extremo cuidado y con el menor número de pasos 
# posibles, para evitar que las dependencias ocasionen que se tenga que 
# actualizar una versión anterior de algún paquete (y por lo tanto 
# se borren archivos) y entonces se rompa la instalación de conda y/o pip. 

# La sintaxis es algo obsoleta, en particular en el uso de los argumentos 
# pues la versión de Docker en el servidor es la 1.13 (ésta es la razón por la 
# que todo está insertado en el código, en lugar de pasarse como
# argumento por línea de comando).

# Tampoco es posible utilizar el multistage build disponible en versiones 
# posteriores de Docker. 

# -----------------------------------------------------------------------------
# Construcción del ambiente base
#   Los argumentos han sido eliminados y nos quedamos con las versiones listadas
#   en el contenedor base de TensorRT (tensorrt:21.03-py): 
#   https://docs.nvidia.com/deeplearning/tensorrt/container-release-notes/rel_21-03.html#rel_21-03
# 
# Se toma el DockerFile del proyecto pytorch como base para instalación de 
# requerimientos: https://github.com/pytorch/pytorch/blob/master/Dockerfile
# -----------------------------------------------------------------------------

FROM nvcr.io/nvidia/tensorrt:21.03-py3

ENV DEBIAN_FRONTEND=noninteractive TZ=America/Mexico_City

# Se agrega el || : para darle la vuelta al problema de los certificados por 
# la hora desfasada del servidor. 
RUN apt-get update || :
RUN apt-get install -y --no-install-recommends \
        ccache \
        git-lfs \
        libsndfile1 \
        libjpeg-dev \
        libpng-dev \
        ffmpeg && \
    rm -rf /var/lib/apt/lists/* || :
RUN /usr/sbin/update-ccache-symlinks
RUN mkdir /opt/ccache && ccache --set-config=cache_dir=/opt/ccache
ENV PATH /opt/conda/bin:$PATH

# Instalación de miniconda
ENV PYTHON_VER=3.8
RUN curl -fsSL -v -o ~/miniconda.sh -O  https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh  && \
    chmod +x ~/miniconda.sh && \
    ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda config --add channels conda-forge;
# El siguiente comando fue omitido pues borrar archivos genera errores por 
# lo descrito en el comentario inicial
# /opt/conda/bin/conda clean -ya;

# Argumentos de TORCH CUDA
ENV TORCH_CUDA_ARCH_LIST="3.7+PTX;5.0;6.0;6.1;7.0;7.5;8.0;8.6"
ENV TORCH_NVCC_FLAGS="-Xfatbin -compress-all"
ENV CMAKE_PREFIX_PATH="$(dirname $(which conda))/../"

# -----------------------------------------------------------------------------
# Instalación de pytorch y los faltantes de la lista del Contenedor de NEMO: 
#   jupyter, tensorboard...
# -----------------------------------------------------------------------------

# Instalación de pip
RUN apt-get install python3-pip

# Instalación de Nemo. Le dejamos la instalación de todos los prerrequisitos a 
# pip con nemo, pues de otra forma deja archivos fantasmas por el bug de docker
# en el servidor. 
RUN pip install Cython 
RUN pip install nemo_toolkit['all'] jupyterlab tensorboard

# Script para lanzar jupyter-notebook (Usa el puerto 8888 por defecto)
RUN printf "#!/bin/bash\njupyter-notebook --no-browser --allow-root --ip=0.0.0.0" >> start-jupyter.sh && \
    chmod +x start-jupyter.sh

